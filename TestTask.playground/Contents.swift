// Short test playground for Curogram's iOS candidates.

import UIKit


/**
 ---------------------------------------------------------------------------------
 How long are you in iOS coding?
 */
let iOSCodingSummary: TimeInterval = 0

print(iOSCodingSummary)



/**
 ---------------------------------------------------------------------------------
 How much Swift experience do you have?
 */
let swiftCodingTime: TimeInterval = 0

print(swiftCodingTime)



/**
 ---------------------------------------------------------------------------------
 Your favourite Swift frameworks
 */
let frameworks = [URL]()

print(frameworks)



/**
 ---------------------------------------------------------------------------------
 What would you prefer to use for your next project?
 */
let pattern = ["MVC", "MVVM"].first ?? ["MVC", "MVVM"].last

print(pattern!)



/**
 ---------------------------------------------------------------------------------
 Complete this function.
 */
func getSomeValue(from some: String?) -> String {
    
}



/**
 ---------------------------------------------------------------------------------
 Complete this function.
 */
func convertValue(from some: Any!) -> String {
    
}



/**
 ---------------------------------------------------------------------------------
 Complete this function.
 */
func convertArray<T>(from inArr: [Any]) -> T {
    
}



/**
 ---------------------------------------------------------------------------------
 Lets say you have to make a lot of API calls. Most of parameters can by equal, but it's values differs case to case.
 
 Here is a sample set of API settings you'd need to pass to each request:
 
 API path:
 https://mytestserver.com/api/{userName}/somethingthatweneed
 
 Method: PUT
 
 Parameters:
 authToken:      {String}
 itemsNumber:    {Int}
 pageSize:       {Int}
 someValue1:     {String}
 someValue2:     {Bool}
 
 
 What would be the best approach of coding an easy-to-use API definitions list holder, for your opinion?
 REMINDER: Target object (list holder) may contain many API calls definitions with different parameters for each item.
 */



/// *** Implementation goes here ***




/**
 ---------------------------------------------------------------------------------
 Implement adding some random string to `mainArray` in each cycle step.
 */
class MyObject {
    
    /// This array can be accessed in main queue only!!!
    var mainArray = [String]()
    
    func startBackgroundTask() {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            while true {
                Thread.sleep(forTimeInterval: 0.5)
                
                // ... do some work here
                
                // *** Your code here ***
            }
        }
    }
}


/// *** Implementation goes here ***




/**
 ---------------------------------------------------------------------------------
 If you had a choice, would you use this "Factory" for creating UI controls or you would 
 like to make it another way. Justify your answer.
 */

enum LabelFactory {
    
    case standardLabel(text: String, textColor: UIColor, fontStyle: UIFontTextStyle, textAlignment: NSTextAlignment?, sizeToFit: Bool, adjustToFit: Bool)
    
    var new: UILabel {
        
        switch self {
        case .standardLabel(let text,let textColor,let fontStyle, let textAlignment,let sizeToFit, let adjustToFit):
            return createStandardLabel(text: text, textColor: textColor, fontStyle: fontStyle, textAlignment: textAlignment, sizeToFit: sizeToFit, adjustToFit: adjustToFit)
        }
    }
    
    private func createStandardLabel(text: String, textColor: UIColor, fontStyle: UIFontTextStyle, textAlignment: NSTextAlignment?, sizeToFit: Bool, adjustToFit: Bool) -> UILabel {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = adjustToFit
        label.text = text
        label.font = UIFont.preferredFont(forTextStyle: fontStyle)
        label.textAlignment = textAlignment ?? .left
        label.textColor = textColor
        if sizeToFit {
            label.sizeToFit()
        }
        return label
    }
}


/// *** Answer and/or implementation goes here ***





/**
 ---------------------------------------------------------------------------------
 Synchronize an execution of these three operations. They should be executed one right after another. As a result, you need to output all functions results in a reversed order into existing value.
 */

let queue = OperationQueue()
queue.maxConcurrentOperationCount = Int(Int8.max)

func operation1(with parameter: String, and completion: @escaping ((String) -> Void)) {
    queue.addOperation { 
        Thread.sleep(forTimeInterval: 0.1)
        completion(parameter)
    }
}

func operation2(with amount: UInt, and completion: @escaping ((String) -> Void)) {
    DispatchQueue.global().async {
        var result: UInt = 0
        for i in 0 ..< amount {
            Thread.sleep(forTimeInterval: 0.1)
            result += i
        }
        completion(String(describing: result))
    }
}

let operation3: ((String) -> String) = { (inStr) -> String in
    return inStr
}

/// Main function
func main() {
    /// WARNING: this value can NOT be modified inside any other queue!
    var result = [String]()
    
    // **** Your code goes here ****
    
    print(result)
}

main()



